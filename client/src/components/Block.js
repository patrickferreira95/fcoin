import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Transaction from './Transaction';
import { Card, CardText, CardBody, CardTitle, CardSubtitle, Container, Row, Col } from 'reactstrap';

class Block extends Component {
  state = { displayTransaction: false };

  toggleTransaction = () => {
    this.setState({ displayTransaction: !this.state.displayTransaction });
  }

  get displayTransaction() {
    const { data } = this.props.block;

    const stringifiedData = JSON.stringify(data);

    const dataDisplay = stringifiedData.length > 35 ?
      `${stringifiedData.substring(0, 35)}...` :
      stringifiedData;

    if (this.state.displayTransaction) {
      return (
        <div>
          {
            data.map(transaction => (
              <div key={transaction.id}>
                <hr />
                <Transaction transaction={transaction} />
              </div>
            ))
          }
          <br />
          <Button className="btn-color-4 btn-blocks" onClick={this.toggleTransaction}>
            Mostrar menos
          </Button>
        </div>
      )
    }

    return (
      <div>
        <div>Data: {dataDisplay}</div>
        <Button className="btn-color-4 btn-blocks" onClick={this.toggleTransaction}>
          Mostrar mais
        </Button>
      </div>
    );
  }

  render() {
    const { timestamp, hash } = this.props.block;

    const hashDisplay = `${hash.substring(0, 15)}...`;

    return (
      <div className='Block container'>


          <Col sm="12" md={{ size: 12, offset: 3 }}>
            <Card>
              <CardBody>
                <CardTitle>Hash: {hashDisplay}</CardTitle>
                <CardSubtitle>Timestamp: {new Date(timestamp).toLocaleString()}</CardSubtitle>
                <CardText className='text-size-data'>{this.displayTransaction}</CardText>
              </CardBody>
            </Card>
          </Col>
       
      </div>
    );
  }
};

export default Block;