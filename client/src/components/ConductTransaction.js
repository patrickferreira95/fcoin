import React, { Component } from 'react';
import { FormGroup, FormControl, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import history from '../history';
import Navw from './Navw';
import { Jumbotron } from 'react-bootstrap';

class ConductTransaction extends Component {
  state = { recipient: '', amount: 0, knownAddresses: [] };

  componentDidMount() {
    fetch(`${document.location.origin}/api/known-addresses`)
      .then(response => response.json())
      .then(json => this.setState({ knownAddresses: json }));
  }

  updateRecipient = event => {
    this.setState({ recipient: event.target.value });
  }

  updateAmount = event => {
    this.setState({ amount: Number(event.target.value) });
  }

  conductTransaction = () => {
    const { recipient, amount } = this.state;

    fetch(`${document.location.origin}/api/transact`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ recipient, amount })
    }).then(response => response.json())
      .then(json => {
        alert(json.message || json.type);
        history.push('/transaction-pool');
      });
  }

  render() {
    return (
      <div className='ConductTransaction container'>

        <div className='container margin-nav-conduct'>
            <Navw />
        </div>


        <Jumbotron className='jumbotron'>
          <div className='color-transac'>
              <Link to='/'>
                <Button className="btn-color-4">
                  Voltar ao Início
                </Button>
              </Link>
            <h2>Transação Digital</h2>
            <br />
            <h4>Endereços conhecidos</h4>
            {
              this.state.knownAddresses.map(knownAddress => {
                return (
                  <div key={knownAddress}>
                    <div>{knownAddress}</div>
                    <br />
                  </div>
                );
              })
            }
            <br />
            <FormGroup>
              <FormControl
                input='text'
                placeholder='Destino da transação'
                value={this.state.recipient}
                onChange={this.updateRecipient}
              />
            </FormGroup>
            <FormGroup>
              <FormControl
                input='number'
                placeholder='Quantidade a enviar'
                value={this.state.amount}
                onChange={this.updateAmount}
              />
            </FormGroup>
            <div>
              <Button className="btn-color-4" onClick={this.conductTransaction}>
                Enviar
              </Button>
            </div>
          </div>
        </Jumbotron>
      </div>
      
    )
  }
};

export default ConductTransaction;