import React, { Component } from 'react';
import { Button, Jumbotron } from 'react-bootstrap';
import Transaction from './Transaction';
import { Link } from 'react-router-dom';
import history from '../history';
import Navw from './Navw';

const POLL_INTERVAL_MS = 10000;

class TransactionPool extends Component {
  state = { transactionPoolMap: {} };

  fetchTransactionPoolMap = () => {
    fetch(`${document.location.origin}/api/transaction-pool-map`)
      .then(response => response.json())
      .then(json => this.setState({ transactionPoolMap: json }));
  }

  fetchMineTransactions = () => {
    fetch(`${document.location.origin}/api/mine-transactions`)
      .then(response => {
        if (response.status === 200) {
          alert('success');
          history.push('/blocks');
        } else {
          alert('The mine-transactions block request did not complete.');
        }
      });
  }

  componentDidMount() {
    this.fetchTransactionPoolMap();

    this.fetchPoolMapInterval = setInterval(
      () => this.fetchTransactionPoolMap(),
      POLL_INTERVAL_MS
    );
  }

  componentWillUnmount() {
    clearInterval(this.fetchPoolMapInterval);
  }

  render() {
    return (
      <div className='TransactionPool container margin-nav-transac'>

        
          <div className='container'>
            <Navw />
          </div>

        <Jumbotron className='jumbotron'>
          <div className='margin-transac-inicial'>
          <div className='container margin-transac color-transac'>
            <Link to='/'>
              <Button className="btn-color-4">
                Voltar ao Início
              </Button>
            </Link>
            <div className='margin-transac'>
              <h2>Pool de Transações</h2>
              {
                Object.values(this.state.transactionPoolMap).map(transaction => {
                  return (
                    <div key={transaction.id}>
                      <hr />
                      <Transaction transaction={transaction} />
                    </div>
                  )
                })
              }
              <hr />
              <div className='margin-transac'>
                <Button className="btn-color-4" onClick={this.fetchMineTransactions}>
                  Minerar o Bloco
                </Button>
              </div>
          </div>
          </div>
          </div>
        </Jumbotron>
      </div>
    )
  }
}

export default TransactionPool;