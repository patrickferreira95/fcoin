import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import logo from '../assets/logo-1.png';


export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className = "margin-nav">
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/"><img className='logoNav' src={logo}></img></NavbarBrand>
         
          <Collapse navbar>
            <Nav className="ml-auto margin-items" navbar>
              <NavItem>
                <NavLink href="/blocks">Blockchain</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/conduct-transaction">Inicie uma transação</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/transaction-pool">Pool de transações</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}