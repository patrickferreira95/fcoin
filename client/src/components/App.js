import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/logo.png';
import lateral from '../assets/lateral.png';
import { Button, Image } from 'reactstrap';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Navb from './Navb';

class App extends Component {
  state = { walletInfo: {} };

  componentDidMount() {
    fetch(`${document.location.origin}/api/wallet-info`)
      .then(response => response.json())
      .then(json => this.setState({ walletInfo: json }));
  }

  render() {
    const { address, balance } = this.state.walletInfo;

    return (
      <div className='App body-home'>
        
        <div className='container margin-nav'>
          <Navb />
        </div>

        <img className='lateral' src={lateral}></img>
        
        <div className='container'>
        <div className='margin-content'>
         
          <br />
          <div>
            <h1>Simule uma criptomoeda!</h1>
            <h3 className='margin-h3'>Comece agora a operar com a FCoin!</h3>
          </div>
          <br />
          <div className='wrap'>
            <Link to="/blocks">
              <Button className="btn-color-1">
                Blockchain
              </Button>
            </Link>

            <Link to="/conduct-transaction">
              <Button className="btn-color-2">
                Iniciar uma transação
              </Button>
            </Link>

            <Link to="/transaction-pool">
              <Button className="btn-color-3">
                Pool de Transações
              </Button>
            </Link>
          </div>
          <br />
          <div className='WalletInfo'>
            <div>Seu saldo: f${balance}</div>
            <div>Seu endereço público:</div>
            <p>{address}</p>
          </div>
        </div>
        </div>
      </div>
    );
  }
}

export default App;